#!/bin/bash
outputpath="$(pwd)/bin/Release/publish/"
dotnet clean
dotnet publish -c Release --os linux -p:PublishSingleFile=true --self-contained=true --output=$outputpath
chmod 700 "$outputpath/ChoreScheduler"