# ChoreScheduler

## Overview

ChoreScheduler is a digital chore wheel that keeps track of a given set of users and chores, and emails whoever is responsible for a given chore. It will determine who is responsible for what chore, email each user, then exit each time it is run. It is meant to be run by a chron job or scheduled task weekly.

## Building

### Building the container (recommended)

This is the easiest way to build the application. It will required [podman](https://podman.io/) or [docker](https://www.docker.com/). I recommend podman, especially if you are trying to do this on a Windows device. At the time of writing the supported method for using Docker on windows is the non-free/non-libre Docker Desktop software. Podman on Windows is free/libre, and can be installed by [enabling wsl2](https://learn.microsoft.com/en-us/windows/wsl/install) and running `winget install podman`.

_Please note that no part of the build or application has been tested on Windows. Some modification may be required._

1. Clone the repository to a folder on the machine you intend to run it on
2. Open a terminal and CD to the repository folder
3. Run `podman build . -t chorescheduler` or (if using Docker) `sudo docker build . -t chorescheduler`

### Building the executable directly

This will require the dotnet 7 SDK, and the computer you are running it on will need the dotnet 7 runtime. Both are available at <https://dotnet.microsoft.com/en-us/download/dotnet/7.0>, but I would recommend installing them through your package manager if available.

1. Clone the repository to a folder
2. Open a terminal and CD to the repository folder
3. Run `./build.sh` (if you are building for Windows you can run `dotnet publish -c Release --os win -p:PublishSingleFile=true --self-contained=true`)

## Installation

Installation and usage of this application requires basic knowledge of the command line and file permissions. The easiest way to set it up is definitely the container. This project is designed and written on a Linux system, but should run fine on other platforms.

### Container (recommended)

Copy the contents of the repositories Config folder to a desired location. I recommend `~/.ChoreScheduler` on Linux, or `%appdata%\ChoreScheduler` on Windows (again this has not been tested in Windows, you may want to pick a folder that is easier for WSL2 to find).

We will be using volume mapping to point the container to your config directory, so it can be anywhere you like. If you are using podman rootless (which is ideal) you just need to make sure the user running the application has read access to these files. If you are putting them in one of the above recommended locations this won't be an issue.

In your crontab config use `podman run -v ~/.ChoreScheduler:/etc/ChoreScheduler registry.gitlab.com/kaisev/nix-scripts/chorescheduler Prod` as run command. Of course use `docker` instead of `podman` if you are using Docker.

### Baremetal

Copy the ChoreScheduler executable to an appropriate location, such as `/usr/local/sbin/ChoreScheduler` or `C:\Program Files\ChoreScheduler`. On nix hosts ensure that it has execute permissions, if you are using the recommended path you can run `chmod 700 /usr/local/sbin/ChoreScheduler`

The application automatically searches for configuration files in the following directories:
- `/etc/ChoreScheduler`
- `/usr/local/etc/ChoreScheduler`
- `~/.ChoreScheduler`
- `%AppData%\ChoreScheduler`
- `%LocalAppData\ChoreScheduler`

## Configuration

In whichever config directory you choose to use you will need a folder named `chores` and files named `users.csv`, `adminemail.txt`, and `mailcommand.txt`. The file structure will appear as below:

```
ChoreScheduler
              ┣ adminemail.txt
              ┣ mailcommand.txt
              ┣ users.csv
              ┗ chores
                      ┣ 1Bathroom.html
                      ┣ 2Garbage.html
                      ┣ 3Kitchen.html
                      ┗ 4LivingRoom.html
```

### The adminemail.txt file

This is a plain text file with nothing but the email address of the administrator in it. An example is below:

``` default
admin@example.com
```

If you are setting this up, this should probably be your email address.

### The mailcommand.txt file

This file contains the command to be used to send an email to the end user. This file can be a script, or it can refer to an external program/script. The best option is usually to use an existing email account. In place of the email to send to, the subject, and the email body use `{to}`, `{subject}`, and `{body}`.

The below example can be used with a gmail account on a nix machine (you must set up an app password in your account). Please ensure the security on the file is set correctly, `chmod 600` when running as a secure user. In Windows I would suggest using PowerShell and an addon for interfacing with Credential Manager to protect your credentials.

**If you are using the container in Windows you can use the Linux example below.**

#### Linux Example

The below example using curl has been tested and is known to work

``` bash
body="From: Chore Scheduler <youremail@gmail.com\nTo: <{to}>\nSubject: {subject}\nReply-To: Chore Scheduler <youremail@gmail.com>\nCc:\nMIME-Version: 1.0\nContent-Type: text/html\n\n{body}"
curl -s \
    --url 'smtps://smtp.gmail.com:465' --ssl-reqd \
    --mail-from 'youremail@gmail.com' \
    --mail-rcpt '{to}' \
    --user 'youremail@gmail.com:yourapppassword' \
    -T <(echo -e $body)
```

#### Windows example

The below is an example of how you could call another script if you are running baremetal on Windows.

``` PowerShell
C:\Windows\System32\PowerShell.exe -Command "C:\path\to\script.ps1 -To \"{to}\" -Subject \"{subject}\" -Body \"{body}\""
```

### The chores folder

The chores folder has HTML files in it. The name of the file will become the name of the chore, and the contents are the HTML formatted description that gets emailed to the user. The order in which chores are assigned depends on the order of users in users.csv, and the *alphabetical order of these files*. Any number of digits can be added to the beginning of the file name for sorting, and will be removed from the chore name before sending the emails.

An example for 1Bathroom.html is included below:

``` html
<h1>Bathroom</h1>
<h2>Bare Minimum</h2>

<ol>
    <li>Toilet, seat, and exterior cleaned and disinfected</li>
    <li>Inside of toilet bowl disinfected/scrubbed</li>
    <li>Counter and sink disinfected</li>
    <li>Tub scrubbed with melamine sponge and rinsed</li>
    <li>Floor swept and mopped</li>
</ol>

<h2>Extras</h2>

<ol>
    <li>Mirror cleaned</li>
    <li>Fan vent cleaned</li>
    <li>Hair removed from tub drain</li>
    <li>Bath mat washed and dried (The washer will not balance it on its own, add at least its own weight in other articles)</li>
</ol>
```

### The users.csv file

This is a csv file with two columns, for the user's name and their email address. One user per line, no headings.

This file can be created using a spreadsheet editor like LibreOffice calc or Microsoft Office Excel, but due to the simplicity of the file it is likely best to use a text editor such as notepad++, kate, nano, vim, or notepad. If you choose to use Excel make sure to save the file as **CSV (comma delimited)**. If you choose tab delimited it will save it as a TSV with the CSV extension, which will not work.

Do not include commas in the user's name or email, ChoreScheduler does not currently respect quotes so there is no way to add commas without it being misread.

The file should appear as below in a text editor:

``` csv
Stacy,stacy@example.com
Stacy's mom,gotitgoingon@example.com
```
While the alphabetical order of the chores files determines in what order a single user will be assigned chores, the order of users in this file will determine the order in which your users each do a single chore. If you had a paper chore wheel, modifying the order of the chore files would be the same as changing the order of the chores on the wheel, changing the order of the users in this file is the same as changing the order of the users on the wheel.

## Running the application

There are three ways to run ChoreScheduler, two for testing and one for production:

1. Run `ChoreScheduler`, the program will write the users and their assignments to the console
2. Run `ChoreScheduler TestEmail`, this will send all emails to the test email address specified in `adminemail.txt`
3. Run `ChoreScheduler Prod`, this will write the users and their assignments to the console and send the emails to the appropriate addresses

These options can also be chosen by setting the `TESTLEVEL` environment variable, though if an argument is supplied it will always overrule the environment variable.

On Windows open task scheduler and create a new task that starts `podman run -v \path\to\config\ChoreScheduler:/etc/ChoreScheduler/ chorescheduler prod` (or if you're not using a container `C:\Program Files\ChoreScheduler\ChoreScheduler.exe prod`) on a weekly basis.

On nix run `crontab -e` and add the appropriate line:

### Podman

`0 8 * * 1 podman run -v ~/ChoreScheduler:/etc/ChoreScheduler/ chorescheduler prod >> ~/ChoreScheduler/runs.log 2>&1`

### Docker

Remember you have to run Docker as root, so make sure you ran `crontab -e` as root.

`0 8 * * 1 docker run -v /home/<your username>/ChoreScheduler:/etc/ChoreScheduler/ chorescheduler prod >> /home/<your username>/ChoreScheduler/runs.log 2>&1`

### Baremetal

`0 8 * * 1 /usr/local/sbin/ChoreScheduler Prod >> ~/ChoreScheduler/runs.log 2>&1`
