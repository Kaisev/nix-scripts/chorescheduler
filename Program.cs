﻿using System;
using System.IO;
using System.Collections.Generic;

namespace ChoreScheduler
{
    // A static class for storing and determining various settings
    public static class Settings
    {
        public static string ConfigPath { get; private set; }
        public static string AdminEmail { get; private set; }
        public static KeyValuePair<string, string>[] Chores;
        public static string[] Users;
        public static string MailCommand;
        public static bool IsLoaded { get; private set; }
        public static bool Load()
        {
            // List of paths to check, in the order in which we want to check for them
            string[] pathstocheck = new string[] {
                Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.UserProfile), ".ChoreScheduler"),
                "/etc/ChoreScheduler",
                "/usr/local/etc/ChoreScheduler",
                Path.Combine(System.Environment.GetEnvironmentVariable("APPDATA") ?? "","ChoreScheduler"),
                Path.Combine(System.Environment.GetEnvironmentVariable("LOCALAPPDATA") ?? "","ChoreScheduler")
            };
            // Iterate through the paths, stop and return the first one that exists
            foreach (string path in pathstocheck)
            {
                if (Directory.Exists(path))
                {
                    // Set the config path
                    ConfigPath = path;

                    // Get the chorenames and descriptions
                    List<KeyValuePair<string, string>> chores = new List<KeyValuePair<string, string>>();

                    // List the files in the chores directory
                    List<string> files = new List<string>(Directory.EnumerateFiles(Path.Combine(path, "chores")));

                    // Sort the files alphabetically
                    files.Sort((x, y) => String.Compare(x, y));

                    // Iterate through the files, removing the leading digits from the names and adding the chores to the list
                    foreach (string file in files)
                    {
                        string name = Path.GetFileNameWithoutExtension(file);
                        string description = File.ReadAllText(file);

                        // Remove leading digits
                        name = System.Text.RegularExpressions.Regex.Replace(name, @"^\d+", "");

                        chores.Add(new KeyValuePair<string, string>(name, description));
                    }

                    // Set the public Chores array
                    Chores = chores.ToArray();

                    // Get the users
                    Users = File.ReadAllLines(Path.Combine(path, "users.csv"));

                    // Get the admin email
                    AdminEmail = File.ReadAllText(Path.Combine(path, "adminemail.txt")).Trim();

                    // Get the command string
                    MailCommand = File.ReadAllText(Path.Combine(path, "mailcommand.txt"))
                        .Replace("\"", "\\\"")
                        .Replace(@"{to}", @"{0}")
                        .Replace(@"{subject}", @"{1}")
                        .Replace(@"{body}", @"{2}");

                    IsLoaded = true;

                    return IsLoaded;
                }
            }
            IsLoaded = false;

            return IsLoaded;
        }

    }
    class User
    {
        public string name;
        public string email;

        // Initialize and set variables
        public User(string Name, string Email)
        {
            name = Name;
            email = Email;
        }
    }
    class Chore
    {

        public string Name;

        public string Description;

        public User assignedUser;

        public Chore(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
    static class Chores
    {
        public static Chore[] GetChores()
        {

            // Create the array to return
            Chore[] retval = new Chore[Settings.Chores.Length];

            // Add the details from each file to a new chore in an array
            for (int i = 0; i < Settings.Chores.Length; i++)
            {
                retval[i] = new Chore(Settings.Chores[i].Key, Settings.Chores[i].Value);
            }
            return retval;
        }
    }
    static class weekmath
    {
        static public int GetWeekNumber(int weeks)
        {
            int retval = weeks % Settings.Chores.Length;
            if (retval == 0)
            {
                retval = Settings.Chores.Length;
            }
            return retval;
        }
    }
    class Program
    {
        enum TestLevel
        {
            prod,
            testemail,
            debug
        }
        static void Main(string[] args)
        {
            if (!Settings.Load())
            {
                throw new NotImplementedException("Config directory missing. Please create config directory /etc/ChoreScheduler");
            }

            if (Settings.Users.Length > Settings.Chores.Length) {
                throw new NotImplementedException("There are more users than chores. This situation is not yet supported");
            }

            TestLevel testLevel = TestLevel.debug;

            string arg;

            if (args != null && args.Length > 0) {
                arg = args[0];
            }
            else
            {
                arg = Environment.GetEnvironmentVariable("TESTLEVEL");
            }

            if (arg != null) {
                switch (arg.ToLower())
                {
                    case "prod":
                        Console.WriteLine("Running prod configuration");
                        testLevel = TestLevel.prod;
                        break;
                    case "testemail":
                        Console.WriteLine("Testing mail delivery");
                        testLevel = TestLevel.testemail;
                        break;
                    default:
                        Console.WriteLine(arg + " is not a valid command-line option. Please use \"testemail\" or \"prod\"");
                        testLevel = TestLevel.debug;
                        break;
                }
            }

            // This is the date that this program was first implemented
            // If you're setting this up and don't have an existing schedule to conform to this is arbitrary
            DateTime StartDate = DateTime.Parse("2021-10-25");

            int weeksSinceStart = (DateTime.Now - StartDate).Days / 7;

            // GetWeekNumber is probably completely useless, pretty sure the math would line up the same
            // But meh, it works. Why mess with success
            int weekNumber = weekmath.GetWeekNumber(weeksSinceStart);
            Console.WriteLine("Week number " + weekNumber);

            // Create the users
            User[] users = new User[Settings.Users.Length];

            for (int i = 0; i < Settings.Users.Length; i++)
            {
                string[] splitString = Settings.Users[i].Split(",");
                users[i] = new User(splitString[0], splitString[1]);
            }

            // The object used for the users once they're sorted
            // Users are sorted depending on the weekNumber to determine who gets assigned to what chore
            User[] sortedUsers = new User[users.Length];

            // Shifts the array to the side
            for (int i = 0; i < users.Length; i++)
            {
                // Shifts sortedUsers by which week it is
                sortedUsers[i] = users[(i + weekNumber) % users.Length];
            }
            // Create the chores objects
            Chore[] chores = Chores.GetChores();

            // Set users to their chores
            for (int i = 0; i < sortedUsers.Length; i++)
            {
                chores[i].assignedUser = sortedUsers[i];
            }
            // Process the chores
            foreach (var chore in chores)
            {
                Console.WriteLine((chore.assignedUser?.name ?? "Nobody") + " assigned to " + chore.Name);
                if (chore.assignedUser == null) {
                    continue;
                }
                string subject = $"Your chore this week: {chore.Name}";
                string body = $"Hello {chore.assignedUser.name}, please see below for your chore's definition of 'done': \\n\\n{chore.Description.Replace("\n", "\\n")}";
                string shell;
                if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(System.Runtime.InteropServices.OSPlatform.Windows))
                {
                    shell = @"C:\Windows\System32\cmd.exe";
                }
                else
                {
                    shell = @"/bin/bash";
                }
                // Only send emails if Prod argument is supplied
                if (testLevel == TestLevel.prod)
                {
                    Console.WriteLine($"Sending email to {chore.assignedUser.email}");
                    System.Diagnostics.Process.Start(shell, "-c \"" + string.Format(Settings.MailCommand, chore.assignedUser.email, subject, body) + "\"");
                }
                // Send test emails to the admin's address
                else if (testLevel == TestLevel.testemail)
                {
                    Console.WriteLine($"Sending email to {Settings.AdminEmail}");

                    System.Diagnostics.Process.Start(shell, "-c \"" + string.Format(Settings.MailCommand, Settings.AdminEmail, subject, body) + "\"");
                }
            }
        }
    }
}